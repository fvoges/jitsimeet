# @summary Manages configuration file for the difference services.
#
class jitsimeet::config {

  if $jitsimeet::manage_certs {
    # Files are pushed by the prosody module
    $certificates = [ $jitsimeet::jitsi_domain, "auth.${jitsimeet::jitsi_domain}" ]
    $certificates.each |Stdlib::FQDN $cert| {
      letsencrypt::certonly { $cert: }
    }
  }
}
