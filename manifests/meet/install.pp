# @summary Manages the jitsi meet package.
#
class jitsimeet::meet::install {
  package {$jitsimeet::meet::package_name:
    ensure       => $jitsimeet::meet::package_ensure,
    responsefile => '/var/local/jitsimeet.preseed',
  }
}
