# @summary Videobridge subclass.
#
# @param service_name
#   Name of the videobridge service.
#
# @param package_name
#   Name of the videobridge package to be installed.
#
# @param package_ensure
#   State of the videobridge package.
#
class jitsimeet::videobridge (
  String[1] $service_name,
  String[1] $package_name,
  String[1] $package_ensure,
) {
  contain 'jitsimeet::videobridge::install'
  contain 'jitsimeet::videobridge::config'
  contain 'jitsimeet::videobridge::service'

  Class['jitsimeet::videobridge::install']
  -> Class['jitsimeet::videobridge::config']
  -> Class['jitsimeet::videobridge::service']
}
