# @summary Manages the jicofo service.
#
class jitsimeet::jicofo::service {
  service { $jitsimeet::jicofo::service_name:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    provider   => 'systemd',
    require    => Package[$jitsimeet::jicofo::package_name],
  }
}
