# @summary Manages the videobridge configuration.
#
class jitsimeet::videobridge::config {
  file {'/etc/jitsi/videobridge/config':
      ensure  => present,
      content => epp('jitsimeet/jvb-config.epp', {
        'jitsi_domain'       => $jitsimeet::jitsi_domain,
        'jvb_secret'         => $jitsimeet::jvb_secret,
        'jvb_daemon_options' => $jitsimeet::jvb_daemon_options,
      }),
      owner   => 'jvb',
      group   => 'jitsi',
      mode    => '0640',
      notify  => Service[$jitsimeet::videobridge::service_name],
  }

  $_properties = {} + $jitsimeet::videobridge_additional_properties

  $_properties_lines = $_properties.map |$key, $value| {
    "${key}=${value}"
  }

  file { '/etc/jitsi/videobridge/sip-communicator.properties':
    content => $_properties_lines.join("\n")
  }

  file { '/usr/share/jitsi-videobridge/lib/videobridge.rc':
      ensure  => present,
      content => epp('jitsimeet/videobridge.rc.epp', {
        'jvb_max_memory' => $jitsimeet::jvb_max_memory,
      }),
      owner   => 'jvb',
      group   => 'jitsi',
      mode    => '0640',
      notify  => Service[$jitsimeet::videobridge::service_name],
  }

}
