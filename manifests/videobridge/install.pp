# @summary Manages the videobridge package.
#
class jitsimeet::videobridge::install {
  package { $jitsimeet::videobridge::package_name:
    ensure       => $jitsimeet::videobridge::package_ensure,
    responsefile => '/var/local/jitsimeet.preseed',
  }
}
