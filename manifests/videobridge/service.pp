# @summary Manages the videobridge service.
#
class jitsimeet::videobridge::service {
  service { $jitsimeet::videobridge::service_name:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    provider   => 'systemd',
    require    => Package[$jitsimeet::videobridge::package_name],
  }
}
